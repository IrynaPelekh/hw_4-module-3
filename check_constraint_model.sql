USE auto_show
GO

--SELECT * FROM car

INSERT INTO car (model, drive, height_mm, width_mm, wheelbase_mm, 
   length_mm, engine_layout, cylinders_number,     
   engine_displacement, power_kW, top_speed_kmh, trunk_capacity,   
   fuel_tank, color, doors_number, price_usd, special_model, 
   inserted_date)

VALUES  ('Porsche Panamera', 'Rear wheel drive', 1423, 1937, 
        2950, 5049, 'Front Engine', 6, 2995, 243, 264, 500, 
        75, 'black', 5, 115000, 0, getdate())