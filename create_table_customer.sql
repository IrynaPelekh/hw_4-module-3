USE [auto_show]
go

CREATE TABLE [customer] (
  contract_number int PRIMARY KEY IDENTITY,
  car_id int,     
  quantity int,
  customer_price_usd decimal(15,2),
  client_id int,
  order_price AS customer_price_usd * quantity,
  order_date datetime,
  updated_date datetime default GETDATE()

CONSTRAINT  FK_customer_order   FOREIGN KEY  (car_id)
REFERENCES car (id) ON DELETE  CASCADE ON UPDATE  SET NULL
)